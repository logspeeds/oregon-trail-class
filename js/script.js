/*=============================================================================================================
..............................................TRAVELER...........................................................
===============================================================================================================*/

//COMO CRIAR UM VIAJANTE: let nomeDoViajante = new Traveler ("nomeDoViajante")

class Traveler {

    constructor(name, food, isHealthy) {
        this.name = name;
        this.food = 1;
        this.isHealthy = true;
    }

    //===================//
    //--hunt(TRAVELER)---//
    //===================//

    // COMO CAÇAR: nomeDoViajante.hunt()

    //Aumenta a comida do viajante em 2.

    hunt() {
        const huntFood = this.name + " encontrou comida. (+2)";
        this.food += 2;

        return huntFood
    }

    //===================//
    //---eat(TRAVELER)---//
    //===================//

    // COMO COMER: nomeDoViajante.eat()

    /*Consome 1 unidade da comida do viajante. Se o viajante não tiver comida
     para comer, ele deixa de estar saudável.*/

    eat() {
        let foodStatus = "";

        if (this.food > 1) {
            foodStatus = this.name + " se alimentou. (-1)";
            this.food -= 1;
        }
        else if (this.food == 1) {
            foodStatus = this.name + " comeu seu último alimento mas ficou doente. (-1)";
            this.food = 0;
            this.isHealthy = false;
        }

        else {
            foodStatus = this.name + " não possui comida.";
        }
        return foodStatus
    }
}

/*=============================================================================================================
..............................................DOCTOR...........................................................
===============================================================================================================*/

//COMO CRIAR UM MÉDICO: let nomeDoMedico = new Doctor ("nomeDoMedico")

//Um Médico é um Viajante com um método adicional "heal"

class Doctor extends Traveler {

    //===================//
    //---heal(DOCTOR)----//
    //===================//

    // COMO CURAR: nomeDoMedico.heal(NomeDoDoente)

    heal(Traveler) {

        //Avalia a necessidade de cura

        let cure = "";

        if (Traveler.isHealthy == true) {
            cure = Traveler.name + " não está doente.";
        }

        else {
            cure = this.name + " curou " + Traveler.name + ".";
            Traveler.isHealthy = true;
        }
        return cure
    }
}

/*=============================================================================================================
..............................................HUNTER...........................................................
===============================================================================================================*/

//COMO CRIAR UM CAÇADOR: let nomeDoCaçador = new Hunter ("nomeDoCaçador")

/*Um Caçador é um Viajante que se dá melhor encontrando comida, mas também 
precisa de mais comida. Ele começa com 2 comidas em vez de apenas 1 como os 
outros viajantes. Ele também pode dar comida para outros viajantes*/

class Hunter extends Traveler {

    constructor(name) {
        super();
        this.food = 2;
    }

//===================//
//---hunt(HUNTER)----//
//===================//

// COMO CAÇAR: nomeDoCaçador.hunt()

    hunt() {
        const huntFood = this.name + " encontrou comida. (+5)";
        this.food += 5;

        return huntFood
    }

//===================//
//---eat(HUNTER)-----//
//===================//

// COMO COMER: nomeDoCaçador.eat()

    eat() {

        //Avalia a quantidade de comida antes de comer

        let foodStatus = "";

        if (this.food > 2) {
            foodStatus = this.name + " se alimentou. (-2)";
            this.food -= 2;
        }

        else if (this.food == 2) {
            foodStatus = this.name + " comeu seu último alimento mas ficou doente. (-2)";
            this.food = 0;
            this.isHealthy = false;
        }

        else if (this.food == 1) {
            foodStatus = this.name + " comeu seu último alimento mas ficou doente. (-1)";
            this.food = 0;
            this.isHealthy = false;
        }

        else {
            foodStatus = this.name + " não possui comida.";
        }
        return foodStatus
    }

//===================//
//--giveFood(HUNTER)-//
//===================//

// COMO DAR COMIDA: nomeDoCaçador.giveFood(nomeDoReceptor, QuantidadeDeComida)

    giveFood(Traveler, numOfFoodUnits) {

        //Avalia a quantidade de comida antes de transferir

        let transferStatus = "";

        if (this.food < numOfFoodUnits) {
            transferStatus = this.name + " não pode transferir essa quantidade de alimento";
        }

        else {
            this.food -= numOfFoodUnits;
            Traveler.food += numOfFoodUnits;
            transferStatus = this.name + " transferiu " + numOfFoodUnits + " unidade(s) de alimento(s) para " + Traveler.name + ".";
        }
        return transferStatus
    }
}
/*=============================================================================================================
..............................................WAGON...........................................................
===============================================================================================================*/

//COMO CRIAR UMA CARROÇA: let nomeDaCarroça = new Wagon (capacidade)

class Wagon {

    constructor(capacity, passengers) {
        this.capacity = capacity;
        this.passengers = [];
    }

    //===============================//
    //--getAvailableSeatCount(WAGON)-//
    //===============================//

    // COMO AVALIAR A QUANTIDADE DE ACENTOS LIVRES: nomeDaCarroça.getAvailableSeatCount()

    /*Retorna o número de assentos vazios, determinado pela capacidade que
     foi estipulada quando a carroça foi criada comparado com o número de 
    passageiros a bordo no momento.*/

    getAvailableSeatCount() {
        let availableSeatCount = "Há " + this.capacity + " assentos vazios na carroça";
        return availableSeatCount
    }

    //===============================//
    //--------join(WAGON)------------//
    //===============================//

    // COMO ADICIONAR UM PERSONAGEM NA CARROÇA: nomeDoVagao.join(nomeDoPersonagem)

    /*Adicione o viajante à carroça se tiver espaço. Se a carroça já estiver 
    cheia, não o adicione.*/

    join(Traveler) {
        if (this.capacity > 0) {
            this.capacity -= 1;
            this.passengers.push(Traveler);

        }
        else {
            let noAvailableSeatCount = "Não há assentos vazios na carroça";
            return noAvailableSeatCount
        }
    }

    //===============================//
    //----shouldQuarantine(WAGON)----//
    //===============================//

    // VERIFICAR SE A CARROÇA DEVE FICAR EM QUARENTENA: nomeDoVagao.shouldQuarantine()

    /*Retorna true se houver pelo menos uma pessoa não saudável na carroça. 
    Retorna false se não houver.*/

    shouldQuarantine() {

        let quarantine = false;
        let who = "";

        for (let i = 0; i < this.passengers.length; i++) {

            if (this.passengers[i]["isHealthy"] == true) {
                quarantine = false
            }

            else {
                quarantine = true;
                who = this.passengers[i]["name"]
                i = this.passengers.length
            }
        }

        //Escreve se a carroça deve ficar em quarentena ou não

        let quarantineString = "";

        if (quarantine == true) {

            quarantineString = "A carroça está em quarentena. " + who + " está doente."
        }

        else {
            quarantineString = "A carroça não possui doentes."
        }

        return quarantineString
    }

    /*===============================//
    //------totalFood(WAGON)---------//
    //===============================//
    
    // VERIFICAR O TOTAL DE COMIDA DA CARROÇA: nomeDoVagao.totalFood()
    
        /*Retorna true se houver pelo menos uma pessoa não saudável na carroça. 
        Retorna false se não houver.*/
    totalFood() {
        let allFood = 0;

        for (let i = 0; i < this.passengers.length; i++) {

            if (this.passengers[i]["food"] > 0) {
                allFood += this.passengers[i]["food"];
            }

        }

        let allFoodString = "A carroça possui " + allFood + " unidade(s) de Comida.";

        return allFoodString
    }
}

